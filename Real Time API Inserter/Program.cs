﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Written by Josh Majzner 3/6/2019
namespace Real_Time_API_Inserter
{
    class Program
    {
        // Enable logging: 
        private static readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();


        static void Main(string[] args)
        {
            logger.Info("Real Time API Inserter initilizing");
            User_Data.statusUpdates su = new User_Data.statusUpdates();
            su.parseJsonForStatusMessages();
            
            for (int i = 0; i < 25; i++)
            {
                su.generateJsonWithAgent("testAgent");
            }

            Console.ReadKey();

        }


        private void unitTests()
        {
            User_Data.statusUpdates su = new User_Data.statusUpdates();

            // Should fail and continue has no data yet: 
            su.returnRandomStatusMessage();

            // future unit test would validate error was returned properly. 

  
            su.InsertStatusMessage("A");
            su.InsertStatusMessage("B");
            su.InsertStatusMessage("C");
            su.InsertStatusMessage("D");
            su.InsertStatusMessage("E");

            // Should return 25 random messages: 
            for (int i = 0; i < 25; i++)
            {
                su.returnRandomStatusMessage();
            }
        }



    }
}
