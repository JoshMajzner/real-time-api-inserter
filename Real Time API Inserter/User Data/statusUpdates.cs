﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Real_Time_API_Inserter.User_Data
{
    class statusUpdates
    {
        // Enable logging: 

        private static readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        List<dynamic> jsonData = new List<dynamic>(); // Dynamic JSON element holding the statusMessages.json element.  NOTE: The element is not utilized at this time. Added for expansion.
        List<string> statusMessages = new List<string>(); // String list of each JSON element in json Data
        List<string> agentIdList = new List<string>(); // Holds all of the agent ID's supplied. 


        public void parseJsonForStatusMessages()
        {
            string json = File.ReadAllText("statusMessages.json");

            dynamic dynJson = JsonConvert.DeserializeObject(json);

            foreach (var item in dynJson)
            {
                jsonData.Add(item);
                InsertStatusMessage(item.ToString());
                logger.Trace("Adding status: " + item);

            }
        }

        public void generateJsonWithAgent(string agentId, bool changeTimeToLocal = false)
        {
            // Build a local copy of the object to manipulate: 
            string jsonObject =  returnRandomStatusMessage();
            dynamic obj = JsonConvert.DeserializeObject(jsonObject);

            // Add in user data: 
            obj.Add("agentId", agentId);

            if (changeTimeToLocal == true)
            {
                // Change the timestamp to the current time in EPOCH MS
                obj.timestamp = genereateTimeStampNow();
            }
            logger.Info(obj);
        }

        private long genereateTimeStampNow()
        {
            DateTime currentDateTime = DateTime.Now;
            long currentDateTimeMilliseconds = new DateTimeOffset(currentDateTime).ToUnixTimeMilliseconds();
            logger.Info(currentDateTimeMilliseconds);
            return (currentDateTimeMilliseconds);

        }



        public void InsertStatusMessage(string statusMessage)
        {
            statusMessages.Add(statusMessage);
        }

        public dynamic returnRandomStatusMessage()
        {
            if (listHasData(statusMessages) == true)
            {
                try
                {
                    var random = new Random();
                    int index = random.Next(statusMessages.Count);
                    //logger.Trace("Returning Status: " + statusMessages[index]);
                    return (statusMessages[index]);
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    return ("ERROR");
                }

            }
            else
            {
                logger.Error("Cannot return a status, no status messages were added yet.");
                return ("ERROR");
            }

        }


        private bool listHasData(List<string> list)
        {
            if (list.Count > 0)
            {
                return (true);
            }
            else
            {
                return (false);
            }
        }
    }

   /// <summary>
   /// Holds the parsed JSON object data to allow easily accessing specific data elements. 
   /// </summary>
    class statusUpdateData
        {
        


        }



}
